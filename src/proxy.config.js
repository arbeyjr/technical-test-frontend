const PROXY_HOST = 'http://localhost:8080';

const PROXY_CONFIG = [
    {
        context: ['/api/**'],
        target: PROXY_HOST,
        changeOrigin: true,
        secure: false,
        logLevel: 'debug'   
    }
]

module.exports = PROXY_CONFIG;
