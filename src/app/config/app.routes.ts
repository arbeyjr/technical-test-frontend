import { Routes } from '@angular/router';
import { TableComponent } from '../UI/pages/person/table/table.component';
import { SearchComponent } from '../UI/pages/person/search/search.component';
import { SummaryComponent } from '../UI/pages/person/summary/summary.component';

export const routes: Routes = [
    {
        path: '',
        component: TableComponent
    },
    {
        path: 'search',
        component: SearchComponent
    },
    {
        path: 'summary',
        component: SummaryComponent
    },
];
