import { ApplicationConfig } from '@angular/core';
import { provideRouter } from '@angular/router';

import { routes } from './app.routes';
import { provideClientHydration } from '@angular/platform-browser';
import { PersonGateway } from '../domain/models/person/gateway/person-gateway';
import { PersonApiService } from '../infraestructure/driven-adapter/person-api/person-api.service';
import { provideHttpClient, withFetch } from '@angular/common/http';

export const appConfig: ApplicationConfig = {
  providers: [
    provideRouter(routes),
    provideClientHydration(),
    provideHttpClient(withFetch()),
    { provide: PersonGateway, useClass: PersonApiService },
  ]
};
