import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable, delay } from 'rxjs';
import { PersonGateway } from '../../../domain/models/person/gateway/person-gateway';
import { RequestPersonModel, ResponsePersonModel } from '../../../domain/models/person/person-model';
import { environment } from '../../../environments/environment';
import { DataEndPoint } from '../../../domain/models/general.models';


@Injectable({
  providedIn: 'root'
})
export  class PersonApiService extends PersonGateway {

  private _url: string;


  constructor(private http: HttpClient) {
    super();
    this._url = `${environment.backend}/person`;
}

  getPersonAll(): Observable<DataEndPoint<Array<ResponsePersonModel>>> {
    const url = `${this._url}/all-persons`;

    return this.http.get<DataEndPoint<Array<ResponsePersonModel>>>(url);
  }

  getPersonFilter(filters: RequestPersonModel): Observable<DataEndPoint<ResponsePersonModel>> {
    let queryParams = new HttpParams();
    queryParams = queryParams.append("documentType", String(filters.documentType));
    queryParams = queryParams.append("documentNumber", String(filters.documentNumber));
    const url = `${this._url}/person-filter`;
    return this.http.get<DataEndPoint<ResponsePersonModel>>(url, { params: queryParams });
  }

}