import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-summary',
  standalone: true,
  imports: [],
  templateUrl: './summary.component.html',
  styleUrl: './summary.component.scss'
})

export class SummaryComponent implements OnInit {

  dataPerson: any;
  constructor(
    private router: Router,
  ) {
    const currentNavigation = this.router.getCurrentNavigation();
    if(currentNavigation && currentNavigation.extras.state){
      let navigation =  currentNavigation.extras.state;
      this.dataPerson = navigation['data'][0];
    } else {
      this.onBack();
    }
  }

  ngOnInit(): void {}


  onBack(){
    this.router.navigate(['/search']);
  }

}