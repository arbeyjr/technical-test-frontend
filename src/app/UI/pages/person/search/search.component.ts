import { CommonModule } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { RequestPersonModel, ResponsePersonModel } from '../../../../domain/models/person/person-model';
import { GetPersonUseCases } from '../../../../domain/usecase/get-person-use-case';
import { DataEndPoint } from '../../../../domain/models/general.models';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { HelpersService } from '../../../shared/helpers/helpers.service';


@Component({
  selector: 'app-search',
  standalone: true,
  imports: [
    CommonModule,
    ReactiveFormsModule,
  ],
  templateUrl: './search.component.html',
  styleUrl: './search.component.scss'
})

export class SearchComponent implements OnInit {

  formSearch!: FormGroup;

  constructor(
    private router: Router,
    private fb: FormBuilder,
    private _getPersonUseCase: GetPersonUseCases,
    private  helpers: HelpersService,
  ) {
    this.formSearch = this.fb.group({
      typeDocument: new FormControl('', [Validators.required]),
      numberDocument: new FormControl('', [Validators.required]),
    });
  }

  ngOnInit(): void {}


  onSearch() {
    this.formSearch.markAllAsTouched();
    if (this.formSearch.valid) {
      let filters: RequestPersonModel = {
        documentType: this.formSearch.get('typeDocument')?.value,
        documentNumber: this.formSearch.get('numberDocument')?.value,
      };

      this._getPersonUseCase.getPersonFilter(filters)?.subscribe({
        next: (resp: DataEndPoint<ResponsePersonModel>) => {
          if(resp.status === 200){
            this.router.navigate(['/summary'], { state: { data: resp.data }, replaceUrl: true });
            this.helpers.toast({ icon: 'success', text: 'Persona encontrada' });

          }
        },
        error: (_error: HttpErrorResponse) => {
          console.log(_error);
          if (_error.status === 404){
            this.helpers.toast({ icon: 'warning', text: _error.error.message });
          } else {
            this.helpers.toast({ icon: 'error', text: _error.error.message });
          }
        },
      });
    }
  }


  onBack(){
    this.router.navigate(['/']);
  }

}