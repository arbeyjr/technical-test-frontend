import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchComponent } from './search.component';
import { RequestPersonModel, ResponsePersonModel } from '../../../../domain/models/person/person-model';
import { DataEndPoint } from '../../../../domain/models/general.models';
import { Router } from '@angular/router';
import { GetPersonUseCases } from '../../../../domain/usecase/get-person-use-case';
import { of } from 'rxjs';

describe('SearchComponent', () => {
  let component: SearchComponent;
  let fixture: ComponentFixture<SearchComponent>;
  let getPersonUseCaseMock: jasmine.SpyObj<GetPersonUseCases>;
  let router: jasmine.SpyObj<Router>;

  beforeEach(async () => {

    getPersonUseCaseMock = jasmine.createSpyObj('GetPersonUseCases', ['getPersonAll', 'getPersonFilter']);
    const routerSpy = jasmine.createSpyObj('Router', ['navigate']);

    await TestBed.configureTestingModule({
      imports: [SearchComponent],
      providers: [
        { provide: GetPersonUseCases, useValue: getPersonUseCaseMock },
        { provide: Router, useValue: routerSpy }
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchComponent);
    component = fixture.componentInstance;
    getPersonUseCaseMock = TestBed.inject(GetPersonUseCases) as jasmine.SpyObj<GetPersonUseCases>;
    fixture.detectChanges();
  });
  
  it('should create', () => {
    expect(component).toBeTruthy();
  });


  it('should navigate to summary page when form is valid and response status is 200', () => {
    const filters: RequestPersonModel = {
      documentType: 'P',
      documentNumber: '123456789',
    };
    const response: DataEndPoint<ResponsePersonModel> = {
      message: 'sucess',
      status: 200,
      data: {}
    };
    getPersonUseCaseMock.getPersonFilter.and.returnValue(of(response));
    spyOn(component.formSearch, 'markAllAsTouched');
    spyOn(router, 'navigate');
  
    component.formSearch.get('typeDocument')?.setValue(filters.documentType);
    component.formSearch.get('numberDocument')?.setValue(filters.documentNumber);
    component.onSearch();
  
    expect(component.formSearch.markAllAsTouched).toHaveBeenCalled();
    expect(getPersonUseCaseMock.getPersonFilter).toHaveBeenCalledWith(filters);
    expect(router.navigate).toHaveBeenCalledWith(['/summary'], {
      state: { data: response.data },
      replaceUrl: true,
    });
  });
  

});