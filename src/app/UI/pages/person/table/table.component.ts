import { Component, OnInit } from '@angular/core';
import { GetPersonUseCases } from '../../../../domain/usecase/get-person-use-case';
import { DataEndPoint } from '../../../../domain/models/general.models';
import { ResponsePersonModel } from '../../../../domain/models/person/person-model';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { HelpersService } from '../../../shared/helpers/helpers.service';


@Component({
  selector: 'app-table',
  standalone: true,
  imports: [],
  templateUrl: './table.component.html',
  styleUrl: './table.component.scss'
})

export class TableComponent implements OnInit {

  dataPersons: Array<ResponsePersonModel> = [];

  constructor(
    private router: Router,
    private _getPersonUseCase: GetPersonUseCases,
    private  helpers: HelpersService,
  ) {
    this.getAllPersons();
  }

  ngOnInit(): void {}

  getAllPersons(): void {
    this._getPersonUseCase.getPersonAll()?.subscribe({
      next: (resp: DataEndPoint<Array<ResponsePersonModel>>) => {
        if(resp.status === 200){
          this.dataPersons = resp.data;
        }
      },
      error: (_error: HttpErrorResponse) => {
        if (_error.status === 404){
          this.helpers.toast({ icon: 'warning', text: _error.error.message });
        } else {
          this.helpers.toast({ icon: 'error', text: _error.error.message });
        }
      },
    });
  }

  onSearch(){
    this.router.navigate(['/search']);
  }
}