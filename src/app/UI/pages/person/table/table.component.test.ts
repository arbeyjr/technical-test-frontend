import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TableComponent } from './table.component';
import { Router } from '@angular/router';
import { GetPersonUseCases } from '../../../../domain/usecase/get-person-use-case';
import { DataEndPoint } from '../../../../domain/models/general.models';
import { ResponsePersonModel } from '../../../../domain/models/person/person-model';
import { of } from 'rxjs';

describe('TableComponent', () => {
  let component: TableComponent;
  let fixture: ComponentFixture<TableComponent>;
  let getPersonUseCaseMock: jasmine.SpyObj<GetPersonUseCases>;
  let router: jasmine.SpyObj<Router>;

  beforeEach(async () => {

    getPersonUseCaseMock = jasmine.createSpyObj('GetPersonUseCases', ['getPersonAll', 'getPersonFilter']);

    const routerSpy = jasmine.createSpyObj('Router', ['navigate']);

    await TestBed.configureTestingModule({
      imports: [TableComponent],
      providers: [
        { provide: GetPersonUseCases, useValue: getPersonUseCaseMock },
        { provide: Router, useValue: routerSpy }
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TableComponent);
    component = fixture.componentInstance;
    getPersonUseCaseMock = TestBed.inject(GetPersonUseCases) as jasmine.SpyObj<GetPersonUseCases>;
    fixture.detectChanges();
  });
  
  it('should create', () => {
    expect(component).toBeTruthy();
  });


  it('should fetch all persons', () => {
    const mockResponse: DataEndPoint<Array<ResponsePersonModel>> = {
      message: 'Success',
      status: 200,
      data: [],
    };
    getPersonUseCaseMock.getPersonAll.and.returnValue(of(mockResponse));
    component.getAllPersons();
    expect(getPersonUseCaseMock.getPersonAll).toHaveBeenCalled();
    expect(component.dataPersons).toEqual(mockResponse.data);
  });


  it('should navigate to Search ', () => {
    component.onSearch();
    expect(router.navigate).toHaveBeenCalledWith(['/search']);
  });
  
});