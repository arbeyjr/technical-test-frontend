export class ResponsePersonModel {
    id?: number;
    documentType?: string;
    documentNumber?: string;
    firstName?: string;
    middleName?: string;
    firstLastName?: string;
    middleLastName?: string;
    phone?: string;
    address?: string;
    cityResidence?: string;
  }


export class RequestPersonModel {
    documentType?: string;
    documentNumber?: string;
  }