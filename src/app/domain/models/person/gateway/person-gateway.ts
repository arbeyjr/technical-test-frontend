import { Observable } from 'rxjs';
import { ResponsePersonModel, RequestPersonModel } from '../person-model';
import { DataEndPoint } from '../../general.models';

export abstract class PersonGateway {

    abstract getPersonAll(): Observable<DataEndPoint<Array<ResponsePersonModel>>>;

    abstract getPersonFilter(filters: RequestPersonModel ): Observable<DataEndPoint<ResponsePersonModel>>;
}