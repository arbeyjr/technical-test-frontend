export class DataEndPoint<T> {
  data: T;
  message: string;
  status: number;

  constructor(type?: T | undefined) {
    this.data = type!;
    this.message = '';
    this.status = 0;
  }
}
