import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { PersonGateway } from '../models/person/gateway/person-gateway';
import { RequestPersonModel, ResponsePersonModel } from '../models/person/person-model';
import { DataEndPoint } from '../models/general.models';

@Injectable({
  providedIn: 'root'
})

export class GetPersonUseCases {

  constructor( private _personGateWay: PersonGateway) {}
  

  getPersonAll(): Observable<DataEndPoint<Array<ResponsePersonModel>>> {
    return this._personGateWay.getPersonAll();
  }

  getPersonFilter(filters: RequestPersonModel): Observable<DataEndPoint<ResponsePersonModel>> {
    return this._personGateWay.getPersonFilter(filters);
  }

}